CREATE DATABASE  IF NOT EXISTS `bd_auxilio`
USE `bd_auxilio`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for entradas
-- ----------------------------
DROP TABLE IF EXISTS `entradas`;
CREATE TABLE `entradas` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `numero_nf` varchar(45) NOT NULL,
  `data_entrada` datetime(6) NOT NULL,
  `qtd_entrada` int(11) NOT NULL,
  `validade_lote` datetime(6) NOT NULL,
  `medicamento_cod` int(11) NOT NULL,
  PRIMARY KEY (`cod`),
  KEY `fk_entradas_medicamento1_idx` (`medicamento_cod`),
  CONSTRAINT `fk_entradas_medicamento1` FOREIGN KEY (`medicamento_cod`) REFERENCES `medicamento` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for itens_retirada
-- ----------------------------
DROP TABLE IF EXISTS `itens_retirada`;
CREATE TABLE `itens_retirada` (
  `qtd_retirada` int(11) NOT NULL,
  `retiradas_cod` int(11) NOT NULL,
  `medicamento_cod` int(11) NOT NULL,
  KEY `fk_itens_retirada_retiradas1_idx` (`retiradas_cod`),
  KEY `fk_itens_retirada_medicamento1_idx` (`medicamento_cod`),
  CONSTRAINT `fk_itens_retirada_medicamento1` FOREIGN KEY (`medicamento_cod`) REFERENCES `medicamento` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_itens_retirada_retiradas1` FOREIGN KEY (`retiradas_cod`) REFERENCES `retiradas` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for medicamento
-- ----------------------------
DROP TABLE IF EXISTS `medicamento`;
CREATE TABLE `medicamento` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `apresentacao` varchar(20) NOT NULL,
  `qtd` int(11) NOT NULL,
  `principio_ativo_cod` int(11) NOT NULL,
  PRIMARY KEY (`cod`),
  KEY `fk_medicamento_principio_ativo_idx` (`principio_ativo_cod`),
  CONSTRAINT `fk_medicamento_principio_ativo` FOREIGN KEY (`principio_ativo_cod`) REFERENCES `principio_ativo` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for operador
-- ----------------------------
DROP TABLE IF EXISTS `operador`;
CREATE TABLE `operador` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `nome_operador` varchar(45) NOT NULL,
  `permissao` varchar(255) DEFAULT NULL,
  `ativo` int(11) DEFAULT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for paciente
-- ----------------------------
DROP TABLE IF EXISTS `paciente`;
CREATE TABLE `paciente` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` int(11) NOT NULL,
  `nome_paciente` varchar(45) NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `data_nasc` datetime(6) NOT NULL,
  `data_cad` datetime(6) NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for principio_ativo
-- ----------------------------
DROP TABLE IF EXISTS `principio_ativo`;
CREATE TABLE `principio_ativo` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `principio_ativo_nome` varchar(25) NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for retiradas
-- ----------------------------
DROP TABLE IF EXISTS `retiradas`;
CREATE TABLE `retiradas` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `data` datetime(6) NOT NULL,
  `operador_cod` int(11) NOT NULL,
  `paciente_cod` int(11) NOT NULL,
  PRIMARY KEY (`cod`),
  KEY `fk_retiradas_operador1_idx` (`operador_cod`),
  KEY `fk_retiradas_paciente1_idx` (`paciente_cod`),
  CONSTRAINT `fk_retiradas_operador1` FOREIGN KEY (`operador_cod`) REFERENCES `operador` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_retiradas_paciente1` FOREIGN KEY (`paciente_cod`) REFERENCES `paciente` (`cod`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

