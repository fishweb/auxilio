<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

$session = new Session();
$user = $session->get('user');

if($user == null  && $_SERVER['REQUEST_URI'] != "/login"){
    $a = new RedirectResponse('/login');
    $a->send();
}
class DefaultController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function homeAction()
    {

        return $this->render('system/home.twig');
    }
}
