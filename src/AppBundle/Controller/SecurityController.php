<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Operador;


class SecurityController extends Controller
{

    /**
     * @Route("/login", name="login")
     */
    public function indexAction(Request $request)
    {
        return $this->render('login.twig', array(
            'last_username' => '',
            'error'         => '',
        ));
    }

    /**
     * @Route("/login_auth", name="login_auth")
     * @Method({"GET", "POST"})
     */
    public function loginAuth(Request $request)
    {
        
        $data = $request->request->all();

        $sql = "SELECT * FROM operador WHERE login = '".$data['_username']."' AND senha = '".$data['_password']."' AND ativo = 1";

        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();

        $login = $conn->query($sql)->fetchAll();

        if($login == []){
            return $this->render('login.twig', array(
                'last_username' => '',
                'error'         => 'Login e senha incorretos!',
            ));
        } elseif(count($login) == 1) {

            $session = new Session();
            $session->set('user', $login[0]['login']);
            $session->set('name', $login[0]['nome_operador']);
            $session->set('role', $login[0]['permissao']);

            $a = new RedirectResponse('/');
            $a->send();

        } else {

            return $this->render('login.twig', array(
                'last_username' => '',
                'error'         => 'Login e senha incorretos!',
            ));

        }

    }

    /**
     * @Route("/logout-asd", name="logout-asd")
     */
    public function logoutAction()
    {

        $session = new Session();
        $session->set('user', null);
        $session->set('name', null);

        $a = new RedirectResponse('/login');
        $a->send();

    }

    /**
     * @Route("/usuarios", name="usuarios")
     */
    public function usuariosAction()
    {

        $sql = "SELECT * FROM operador";

        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();

        $users = $conn->query($sql)->fetchAll();

        return $this->render('system/usuarios/usuarios.twig', [
            'users' => $users
        ]);

    }

    /**
     * @Route("/cadastrar-usuarios", name="cadastrar-usuarios")
     */
    public function cadastrarUsuarios()
    {
        return $this->render('system/usuarios/cadastrar-usuarios.twig');

    }

    /**
     * @Route("/cadastrar-usuarios-action", name="cadastrar-usuarios-action")
     * @Method({"GET", "POST"})
     */
    public function cadastrarUsuariosAction(Request $request)
    {

        //recupera lista de usuário
        $sql = "SELECT * FROM operador";
        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();
        $users = $conn->query($sql)->fetchAll();

        //recupera dados do form
        $data = $request->request->all();

        if(!$data){
            return $this->render('system/pacientes/cadastrar-paciente.twig');
        } else {

            //perssiste operador
            $o = new Operador();
            $o->setNome($data['nome']);
            $o->setLogin($data['usuario']);
            $o->setSenha($data['senha']);
            $o->setPerissao($data['permissao']);
            $o->setAtivo($data['ativo']);

            if($data['senha'] == $data['resenha']) {

                $em = $this->getDoctrine()->getManager();
                $em->persist($o);
                $em->flush();

                return $this->render('system/usuarios/usuarios.twig', [
                    'msg' => 'Usuario cadastrado com sucesso!',
                    'users' => $users
                ]);

            } else {

                return $this->render('system/usuarios/cadastrar-usuarios.twig', [
                    'msg' => 'As senhas não são iguais.',
                ]);
            }

        }

    }

    /**
     * @Route("/editar-usuarios/{id}", name="editar-usuarios")
     * @Method({"GET", "POST"})
     */
    public function editarUsuarioAction(Request $request, $id)
    {

        $data = $request->request->all();

        if(!$data){

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('AppBundle:Operador')->findBy(['cod' => $id]);

            return $this->render('system/usuarios/editar-usuarios.twig', [
                'user' => $user
            ]);
        } else {

            $em = $this->getDoctrine()->getManager();
            $o = $em->getRepository('AppBundle:Operador')->find($id);
            $o->setNome($data['nome']);
            $o->setLogin($data['usuario']);
            $o->setSenha($data['senha']);
            $o->setPerissao($data['permissao']);
            $o->setAtivo($data['ativo']);

            $em->flush();

            $sql = "SELECT * FROM operador";

            $manager = $this->getDoctrine()->getManager();
            $conn = $manager->getConnection();

            $users = $conn->query($sql)->fetchAll();

            return $this->render('system/usuarios/usuarios.twig', [
                'msg' => 'Paciente atualizado com sucesso!',
                'users' => $users
            ]);

        }
    }

}