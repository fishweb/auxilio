<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Entradas;
use AppBundle\Entity\ItensRetirada;
use AppBundle\Entity\Paciente;
use AppBundle\Entity\Retiradas;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;
use HTML2PDF;
use Symfony\Component\Validator\Constraints\Date;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

$session = new Session();
$user = $session->get('user');

if($user == null  && $_SERVER['REQUEST_URI'] != "/login"){
    $a = new RedirectResponse('/login');
    $a->send();
}

class RelatoriosController extends Controller
{
    /**
     * @Route("/relatorio-pacientes", name="relatorio-pacientes")
     */
    public function relatorioPacientes()
    {

        $em = $this->getDoctrine()->getManager();
        $pacientes = $em->getRepository('AppBundle:Paciente')->findAll();

        return $this->render('system/relatorios/relatorio-pacientes.twig', [
            'pacientes' => $pacientes
        ]);
    }

    /**
     * @Route("/relatorio-pacientes/{id}", name="relatorio-pacientes-interna")
     * @Method({"GET", "POST"})
     */
    public function relatorioPacientesAction($id)
    {

        $sql = "
            SELECT * FROM retiradas as r
            INNER JOIN itens_retirada as ir ON r.cod = ir.retiradas_cod
            INNER JOIN paciente as p ON r.paciente_cod = p.cod
            INNER JOIN medicamento as m ON ir.medicamento_cod = m.cod 
            INNER JOIN operador as o ON o.cod = r.operador_cod WHERE r.paciente_cod = ".$id."; 
        ";

        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();

        $result = $conn->query($sql)->fetchAll();
        $paciente = $conn->query('select * from paciente where cod = '.$id)->fetchAll();

        return $this->render('system/relatorios/relatorio-pacientes-action.twig', [
            'result' => $result,
            'paciente' => $paciente
        ]);
    }

    /**
     * @Route("/relatorio-validade", name="relatorio-validade")
     */
    public function relatorioValidade()
    {

        $sql = 'SELECT m.cod, m.nome, numero_nf, data_entrada, qtd_entrada, validade_lote, medicamento_cod, TIMESTAMPDIFF(MONTH , NOW(), validade_lote) AS periodo_meses FROM entradas as e INNER JOIN medicamento as m ON e.medicamento_cod = m.cod;
';

        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();

        $result = $conn->query($sql)->fetchAll();

        $vencimento = [];
        foreach ($result as $r){
            if ($r['periodo_meses'] > 0 && $r['periodo_meses'] <= 2)
                $vencimento[] = $r;
        }

        $vencidos = [];
        foreach ($result as $r){
            if ($r['periodo_meses'] <= 0)
                $vencidos[] = $r;
        }

        return $this->render('system/relatorios/relatorio-validade.twig', [
            'vencimento' => $vencimento,
            'vencidos' => $vencidos
        ]);

    }

    /**
     * @Route("/relatorio-numeros-estoque", name="relatorio-numeros-estoque")
     */
    public function relatorioNumerosEstoque()
    {

        $sql = 'SELECT * FROM medicamento as m INNER JOIN principio_ativo p ON m.principio_ativo_cod = p.cod;';

        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();

        $result = $conn->query($sql)->fetchAll();

        return $this->render('system/relatorios/relatorio-numeros-estoque.twig', [
            'result' => $result
        ]);

    }

    /**
     * @Route("/ultimos-lancamentos", name="ultimos-lancamentos")
     */
    public function ultimosLancamentos()
    {

        $sql = '
          SELECT * FROM entradas as e 
          INNER JOIN medicamento as m ON e.medicamento_cod = m.cod
          ORDER BY data_entrada DESC LIMIT 10;
        ';

        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();

        $entradas = $conn->query($sql)->fetchAll();


        $sql = '
          SELECT * FROM retiradas as r 
          INNER JOIN itens_retirada as ir ON r.cod = ir.retiradas_cod
          INNER JOIN medicamento as m ON ir.medicamento_cod = m.cod
          ORDER BY data DESC LIMIT 10;
        ';

        $manager = $this->getDoctrine()->getManager();
        $conn = $manager->getConnection();

        $retiradas = $conn->query($sql)->fetchAll();

        return $this->render('system/relatorios/ultimos-lancamentos.twig', [
            'entradas' => $entradas,
            'retiradas' => $retiradas
        ]);

    }

    /**
     * @Route("/buscar-relatorio", name="buscar-relatorio")
     * @Method({"GET", "POST"})
     */
    public function buscaPaciente(Request $request){

        $term = $request->request->get('term');

        if($term[0] == '0' or $term[0] == '0' or $term[0] == '1' or $term[0] == '2' or $term[0] == '3' or $term[0] == '4' or $term[0] == '5' or $term[0] == '6' or $term[0] == '7' or $term[0] == '8' or $term[0] == '9'){
            $em = $this->getDoctrine()->getEntityManager();
            $qb = $em->createQueryBuilder();
            $pacientes = $qb->select('p')
                ->from('AppBundle:Paciente', 'p')
                ->where('p.matricula LIKE :term')
                ->setParameter(':term', '%'.$term.'%')
                ->getQuery()->getResult();
        } elseif($term == ''){
            $em = $this->getDoctrine()->getEntityManager();
            $qb = $em->createQueryBuilder();
            $pacientes = $qb->select('p')
                ->from('AppBundle:Paciente', 'p')
                ->getQuery()->getResult();
        } else {
            $em = $this->getDoctrine()->getEntityManager();
            $qb = $em->createQueryBuilder();
            $pacientes = $qb->select('p')
                ->from('AppBundle:Paciente', 'p')
                ->where('p.nome LIKE :term')
                ->setParameter(':term', '%'.$term.'%')
                ->getQuery()->getResult();
        }

        $html = '
        <table class="table table-hover">
                                <tbody id="content-table">
        ';

        $html .= '
            <tr>
                <th>Matrícula</th>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Data de Nascimento</th>
                <th>Data de Cadastro</th>
                <th></th>
            </tr>
        ';

        foreach ($pacientes as $paciente){
            $html .= '
                <tr>
                    <td>'.$paciente->getMatricula().'</td>
                    <td>'.$paciente->getNome().'</td>
                    <td>'.$paciente->getTelefone().'</td>
                    <td>'.date_format($paciente->getDataNasc(), "d/m/Y").'</td>
                    <td>'.date_format($paciente->getDataCad(), "d/m/Y").'</td>
                    <td style="text-align: right;">
                        <a href="relatorio-pacientes/'.$paciente->getCod().'"><button type="button" style="padding: 1px 2px;" class="btn btn-success btn-flat">Retiradas</button></a>
                    </td>
                </tr>
            ';
        }

        $html .= '
        </tbody>
        </table>
        ';


        return new JsonResponse($html);
    }
}